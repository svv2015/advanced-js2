const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function generateBooksList() {
    const root = document.getElementById('root');
    const ul = document.createElement('ul');

    books.forEach((book) => {
      try {
        if (!book.hasOwnProperty('author') || !book.hasOwnProperty('name') || !book.hasOwnProperty('price')) {
          throw new Error(`Об'єкт містить не всі необхідні властивості: ${JSON.stringify(book)}`);
        }

        const li = document.createElement('li');
        li.textContent = `${book.author} - ${book.name}, Price: ${book.price}`;
        ul.appendChild(li);
      } catch (error) {
        console.error(error);
      }
    });

    root.appendChild(ul);
  }

  generateBooksList();

